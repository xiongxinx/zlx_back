package hzvtc.zlx.util;


import hzvtc.zlx.dto.FoodDTO;
import hzvtc.zlx.entity.FoodDO;
import hzvtc.zlx.service.CollectionService;
import hzvtc.zlx.service.CommentService;
import hzvtc.zlx.service.UserService;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * do与dto转换
 *
 * @author 熊新欣
 * @date 2020/8/5
 */
@Setter
public class FoodConvertUtil {
    private UserService userService;
    private CommentService commentService;
    private CollectionService collectionService;

    public FoodConvertUtil(UserService userService) {
        this.userService = userService;
    }

    public FoodConvertUtil(UserService userService, CommentService commentService, CollectionService collectionService) {
        this.userService = userService;
        this.commentService = commentService;
        this.collectionService = collectionService;
    }

    public FoodDTO convert(FoodDO foodDO) {
        FoodDTO foodDTO = new FoodDTO();
        foodDTO.setId(foodDO.getId());
        foodDTO.setFoodName(foodDO.getFoodName());
        foodDTO.setIntroduce(foodDO.getIntroduce());
        foodDTO.setCity(foodDO.getCity());
        foodDTO.setDetail(foodDO.getDetail());

        List<String> picList = new ArrayList<>();
        String pics = foodDO.getPic();
        if(pics != null && !pics.equals("")){
            picList = Arrays.asList(pics.split(","));
        }

        foodDTO.setPic(picList);

        return foodDTO;
    }
}
