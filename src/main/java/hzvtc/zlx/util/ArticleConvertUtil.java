package hzvtc.zlx.util;


import hzvtc.zlx.dto.ArticleDTO;
import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.service.CollectionService;
import hzvtc.zlx.service.CommentService;
import hzvtc.zlx.service.UserService;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;

/**
 * do与dto转换
 * @author 熊新欣
 * @date 2020/8/5
 */
@Setter
public class ArticleConvertUtil {
    private UserService userService;
    private CommentService commentService;
    private CollectionService collectionService;
    public ArticleConvertUtil(UserService userService) {
        this.userService = userService;
    }
    public ArticleConvertUtil(UserService userService, CommentService commentService, CollectionService collectionService) {
        this.userService = userService;
        this.commentService = commentService;
        this.collectionService = collectionService;
    }

    public ArticleDTO convert(ArticleDO articleDO){
        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setId(articleDO.getId());
        articleDTO.setTitle(articleDO.getTitle());
        articleDTO.setAuthor(userService.getUserById(articleDO.getAuthor()));
        if (articleDO.getCoverPicture() != null) {
            articleDTO.setCoverPicture(Constant.URL_PREFIX + articleDO.getCoverPicture());
        }
        articleDTO.setDate(DateFormatUtil.dateFormat(articleDO.getDate()));
        articleDTO.setLikeNumber(articleDO.getLikeNumber());

        if(commentService != null && collectionService != null) {
            articleDTO.setComments(commentService.getCommentsByTargetIdAndModular
                    (articleDO.getId(),Constant.ARTICLE_MODULAR_NUMBER));
            articleDTO.setSaveNumber(collectionService.countCollectionNumber(articleDO.getId()));
            articleDTO.setContent(articleDO.getContent());
        } else {
            articleDTO.setContent(RemoveTagsUtil.getContentWithNoTags(articleDO.getContent()));
        }
        return articleDTO;
    }
}
