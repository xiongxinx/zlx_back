package hzvtc.zlx.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期格式转换
 * @author 熊新欣
 * @date 2020/8/7
 */
public class DateFormatUtil {
    public static String dateFormat(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String dateTimeFormat(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }
}
