package hzvtc.zlx.util;

import hzvtc.zlx.dao.ArticleMapper;
import hzvtc.zlx.dao.FoodMapper;
import hzvtc.zlx.dao.PhotoMapper;
import hzvtc.zlx.dao.SpotMapper;
import hzvtc.zlx.dto.HomeDTO;
import hzvtc.zlx.entity.*;
import hzvtc.zlx.service.UserService;

/**
 * 将数据转换为首页中展示的内容
 * @author 熊新欣
 * @date 2020/8/14
 */
public class HomeConvertUtil {
    private final ArticleMapper articleMapper;
    private final PhotoMapper photoMapper;
    private final SpotMapper spotMapper;
    private FoodMapper foodMapper;
    private UserService userService;

    public HomeConvertUtil(ArticleMapper articleMapper, PhotoMapper photoMapper, SpotMapper spotMapper, FoodMapper foodMapper, UserService userService) {
        this.articleMapper = articleMapper;
        this.photoMapper = photoMapper;
        this.spotMapper = spotMapper;
        this.foodMapper = foodMapper;
        this.userService = userService;
    }

    public HomeDTO getDTO(HomeDO homeDO) {
        if (homeDO.getTypeId() == 1) {
            ArticleDO articleDO = articleMapper.getArticleById(homeDO.getTargetId());
            HomeDTO homeDTO = new HomeDTO();
            homeDTO.setUrl(homeDO.getUrl() + homeDO.getTargetId());
            homeDTO.setType(homeDO.getTypeName());
            return getDTO(articleDO, homeDTO);

        } else if (homeDO.getTypeId() == 2) {
            PhotoDO photoDO = photoMapper.getPhotoById(homeDO.getTargetId());
            HomeDTO homeDTO = new HomeDTO();
            homeDTO.setUrl(homeDO.getUrl() + homeDO.getTargetId());
            homeDTO.setType(homeDO.getTypeName());
            return getDTO(photoDO, homeDTO);

        } else if (homeDO.getTypeId() == 3) {
            SpotDO spotDO = spotMapper.getSpotById(homeDO.getTargetId());
            HomeDTO homeDTO = new HomeDTO();
            homeDTO.setUrl(homeDO.getUrl() + homeDO.getTargetId());
            homeDTO.setType(homeDO.getTypeName());
            return getDTO(spotDO, homeDTO);

        } else if (homeDO.getTypeId() == 4) {
            FoodDO foodDO = foodMapper.getFoodById(homeDO.getTargetId());
            HomeDTO homeDTO = new HomeDTO();
            homeDTO.setUrl(homeDO.getUrl() + homeDO.getTargetId());
            homeDTO.setType(homeDO.getTypeName());
            return getDTO(foodDO, homeDTO);
        } else {
            return null;
        }
    }
    private HomeDTO getDTO(FoodDO foodDO, HomeDTO homeDTO) {
        homeDTO.setTitle(foodDO.getFoodName());
        homeDTO.setImage(foodDO.getPic().split(",")[0]);
        return homeDTO;
    }

    private HomeDTO getDTO(ArticleDO articleDO, HomeDTO homeDTO) {
        homeDTO.setTitle(articleDO.getTitle());
        homeDTO.setUser(userService.getUserById(articleDO.getAuthor()));
        if(articleDO.getCoverPicture() != null) {
            homeDTO.setImage(Constant.URL_PREFIX + articleDO.getCoverPicture());
        }

        return homeDTO;
    }
    private HomeDTO getDTO(PhotoDO photoDO, HomeDTO homeDTO) {
        homeDTO.setContent(photoDO.getContent());
        homeDTO.setUser(userService.getUserById(photoDO.getPostUser()));
        homeDTO.setImage(Constant.URL_PREFIX + photoDO.getImages().split(",")[0]);
        return homeDTO;
    }
    private HomeDTO getDTO(SpotDO spotDO, HomeDTO homeDTO) {
        homeDTO.setTitle(spotDO.getTitle());
        homeDTO.setImage(Constant.URL_PREFIX + spotDO.getImages().split(",")[0]);
        homeDTO.setContent(spotDO.getTags());
        return homeDTO;
    }
}
