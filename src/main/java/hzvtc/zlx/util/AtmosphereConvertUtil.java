package hzvtc.zlx.util;

import hzvtc.zlx.dto.AtmosphereDTO;
import hzvtc.zlx.entity.AtmosphereDO;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Setter
public class AtmosphereConvertUtil {
    public AtmosphereDTO convert(AtmosphereDO atmosphereDO) {
        SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
        AtmosphereDTO atmosphereDTO = new AtmosphereDTO();
        atmosphereDTO.setId(atmosphereDO.getId());
        atmosphereDTO.setCityCultureName(atmosphereDO.getCityCultureName());
        atmosphereDTO.setDescribe(atmosphereDO.getDescribe());
        atmosphereDTO.setCity(atmosphereDO.getCity());
        atmosphereDTO.setTime(ft.format(atmosphereDO.getAddTime()));
        atmosphereDTO.setBusinessHours(atmosphereDO.getBusinessHours());

        List<String> imagelist = new ArrayList<>();
        String images = atmosphereDO.getImages();
        if(images != null && !images.equals("")){
            imagelist = Arrays.asList(images.split(","));
        }

        for(int i = 0; i < imagelist.size(); i++){
            imagelist.set(i, Constant.URL_PREFIX + imagelist.get(i));
        }

        atmosphereDTO.setImages(imagelist);

        return atmosphereDTO;
    }
}
