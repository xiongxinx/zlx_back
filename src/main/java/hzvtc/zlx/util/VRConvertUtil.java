package hzvtc.zlx.util;


import hzvtc.zlx.dto.FoodDTO;
import hzvtc.zlx.dto.VRDTO;
import hzvtc.zlx.entity.FoodDO;
import hzvtc.zlx.entity.VRDO;
import hzvtc.zlx.service.CollectionService;
import hzvtc.zlx.service.CommentService;
import hzvtc.zlx.service.UserService;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

/**
 * do与dto转换
 *
 * @author 熊新欣
 * @date 2020/8/5
 */
@Setter
public class VRConvertUtil {
    private UserService userService;
    private CommentService commentService;
    private CollectionService collectionService;


    public VRDTO convert(VRDO vrdo) {
        VRDTO vrdto = new VRDTO();
        vrdto.setId(vrdo.getId());
        vrdto.setContent(vrdo.getContent());
        vrdto.setImg(Constant.URL_PREFIX+vrdo.getImg());
        vrdto.setUrl(vrdo.getUrl());
        vrdto.setVrName(vrdo.getVrName());
        return vrdto;
    }
}
