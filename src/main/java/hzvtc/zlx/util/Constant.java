package hzvtc.zlx.util;

import org.springframework.stereotype.Component;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Component
public class Constant {
//    public static final String URL_PREFIX = "http://172.18.18.190:8080/";
    public static final String URL_PREFIX = "http://zlx.kikohk.top/";
    public static final String UPLOAD_PATH = "/www/wwwroot/zlx.kikohk.top/upload/";
//    public static final String URL_PREFIX = "http://cdn.kikohk.top/upload/";
//    public static final String UPLOAD_PATH = "E:/upload/";
    public static final Integer PAGE_SIZE = 30;
    public static final Integer ARTICLE_MODULAR_NUMBER = 1;
    public static final Integer PHOTO_MODULAR_NUMBER = 2;

}
