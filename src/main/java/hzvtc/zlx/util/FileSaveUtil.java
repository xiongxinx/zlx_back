package hzvtc.zlx.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * 保存图片
 * @author 熊新欣
 * @date 2020/8/10
 */
public class FileSaveUtil {
    public static String save(MultipartFile file) {
        String fileName = System.currentTimeMillis() + file.getOriginalFilename();
        File img = new File(Constant.UPLOAD_PATH + fileName);
        try {
            file.transferTo(img);
        } catch (IOException e) {
            return null;
        }
        return fileName;
    }
}
