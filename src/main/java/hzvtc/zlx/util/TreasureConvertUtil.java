package hzvtc.zlx.util;


import hzvtc.zlx.dto.ArticleDTO;
import hzvtc.zlx.dto.TreasureDTO;
import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.entity.TreasureDO;
import hzvtc.zlx.service.CollectionService;
import hzvtc.zlx.service.CommentService;
import hzvtc.zlx.service.UserService;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * do与dto转换
 * @author 熊新欣
 * @date 2020/8/5
 */
@Setter
public class TreasureConvertUtil {
    private UserService userService;
    private CommentService commentService;
    private CollectionService collectionService;
    public TreasureConvertUtil(UserService userService) {
        this.userService = userService;
    }
    public TreasureConvertUtil(UserService userService, CommentService commentService, CollectionService collectionService) {
        this.userService = userService;
        this.commentService = commentService;
        this.collectionService = collectionService;
    }

    public TreasureDTO convert(TreasureDO treasureDO){
        TreasureDTO treasureDTO = new TreasureDTO();
        treasureDTO.setId(treasureDO.getId());
        treasureDTO.setTreasureName(treasureDO.getTreasureName());
        treasureDTO.setContent(treasureDO.getContent());
        treasureDTO.setClassifyId(treasureDO.getClassifyId());
        treasureDTO.setPlace(treasureDO.getPlace());
        List<String> picList = new ArrayList<>();
        String imgs = treasureDO.getImg();
        if(imgs != null && !imgs.equals("")){
            picList = Arrays.asList(imgs.split(","));
        }
        List<String> newList = new ArrayList<>();
        for (String list : picList) {
            list = Constant.URL_PREFIX+list;
            newList.add(list);
        }
        treasureDTO.setImg(newList);
        return treasureDTO;
    }
}
