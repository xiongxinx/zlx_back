package hzvtc.zlx.util;

/**
 * 去除富文本的标签，用于展示在文章列表
 * @author 熊新欣
 * @date 2020/8/5
 */
public class RemoveTagsUtil {
    public static String getContentWithNoTags(String content) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < content.length(); i++) {
            char current = content.charAt(i);
            if('<' == current) {
                for (int j = i+1; j < content.length(); j++) {
                    if ('>' == content.charAt(j)) {
                        i = j;
                        break;
                    }
                }
                continue;
            }
            if(current != ' ') {
                result.append(content.charAt(i));
            }
        }
        return result.toString();
    }
}
