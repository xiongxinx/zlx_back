package hzvtc.zlx.controller;

import hzvtc.zlx.entity.PaintingDO;
import hzvtc.zlx.service.PaintingService;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/painting")
@CrossOrigin
@RestController
public class PaintingController {
    private final PaintingService paintingService;

    public PaintingController(PaintingService paintingService) {
        this.paintingService = paintingService;
    }

    @GetMapping("/getlist")
    public Map<String, Object> getList() {
        List<PaintingDO> list = paintingService.listAll();
        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        return map;
    }
}
