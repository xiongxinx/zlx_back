package hzvtc.zlx.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import hzvtc.zlx.dto.PhotoDTO;
import hzvtc.zlx.entity.PhotoDO;
import hzvtc.zlx.service.PhotoService;
import hzvtc.zlx.util.Constant;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 熊新欣
 * @date 2020/8/12
 */
@RestController
@RequestMapping("/photo")
@CrossOrigin
public class PhotoController {
    private final PhotoService photoService;

    public PhotoController(PhotoService photoService) {
        this.photoService = photoService;
    }

    @GetMapping("/getphoto")
    public PhotoDTO getPhotoDetail(@RequestParam("id")Integer id) {
        return photoService.getPhotoById(id);
    }

    @GetMapping("/list")
    public Map<String, Object> listPhoto(@RequestParam(value = "page",required = false,defaultValue = "1") Integer page) {
        Page<PhotoDTO> page1 = PageHelper.startPage(page, Constant.PAGE_SIZE);
        List<PhotoDTO> list = photoService.listPhoto();
        Map<String, Object> map = new HashMap<>(16);
        map.put("pages", page1.getPages());
        map.put("list", list);
        return map;
    }
    @GetMapping("/delete")
    public void deletePhoto(@RequestParam("id") Integer id) {
        photoService.deletePhoto(id);
    }
    @PostMapping("/insert")
    public void insertPhoto(HttpServletRequest request,@RequestParam("size")Integer size, PhotoDO photoDO) throws IOException {
        List<String> imageNames = new ArrayList<>(9);
        String imageNamePrefix = String.valueOf(System.currentTimeMillis());
        for (int i = 0; i < size; i++) {
            MultipartFile file = ((MultipartHttpServletRequest) request).getFile("image"+i);
            if(file != null) {
                String fileName = imageNamePrefix + file.getOriginalFilename();
                file.transferTo(new File(Constant.UPLOAD_PATH + fileName));
                imageNames.add(fileName);
            }
        }
        photoDO.setImages(StringUtils.join(imageNames, ','));
        photoService.insertPhoto(photoDO);
    }
}


