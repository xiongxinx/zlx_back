package hzvtc.zlx.controller;

import hzvtc.zlx.dto.ArticleDTO;
import hzvtc.zlx.entity.CollectionDO;
import hzvtc.zlx.service.CollectionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/7
 */
@RequestMapping("/collection")
@CrossOrigin
@RestController
public class CollectionController {
    private final CollectionService collectionService;

    public CollectionController(CollectionService collectionService) {
        this.collectionService = collectionService;
    }

    @GetMapping("/insert")
    public void insertCollection(CollectionDO collectionDO) {
        collectionService.insertCollection(collectionDO);
    }
    @GetMapping("/delete")
    public void deleteCollection(CollectionDO collectionDO) {
        collectionService.deleteCollection(collectionDO);
    }
    @GetMapping("/list")
    public List<ArticleDTO> listCollectionByUserId(@RequestParam("id") Integer id) {
        return collectionService.listCollectionByUserId(id);
    }
}
