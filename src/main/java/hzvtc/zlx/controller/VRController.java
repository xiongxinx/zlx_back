package hzvtc.zlx.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import hzvtc.zlx.dto.FoodDTO;
import hzvtc.zlx.dto.VRDTO;
import hzvtc.zlx.entity.FoodDO;
import hzvtc.zlx.entity.VRDO;
import hzvtc.zlx.service.FoodService;
import hzvtc.zlx.service.VRService;
import hzvtc.zlx.util.Constant;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@RequestMapping("/vr")
@CrossOrigin
@RestController
public class VRController {
    private final VRService vrService;

    public VRController(VRService vrService) {
        this.vrService = vrService;
    }

    @PostMapping("/insert")
    public Boolean insertVR(VRDO vrdo) {
        return vrService.insertVR(vrdo);
    }

    @GetMapping("/getlist")
    public Map<String, Object> getList(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        Page<VRDTO> page1 = PageHelper.startPage(page, Constant.PAGE_SIZE);
        List<VRDTO> list = vrService.listAll();
        Map<String, Object> map = new HashMap<>(16);
        map.put("pages", page1.getPages());
        map.put("list",list);
        return map;
    }


}
