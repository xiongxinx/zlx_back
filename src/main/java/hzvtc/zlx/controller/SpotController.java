package hzvtc.zlx.controller;

import hzvtc.zlx.dto.SpotDTO;
import hzvtc.zlx.service.SpotService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/12
 */
@RestController
@RequestMapping("spot")
@CrossOrigin
public class SpotController {
    private final SpotService spotService;

    public SpotController(SpotService spotService) {
        this.spotService = spotService;
    }
    @GetMapping("/list")
    public List<SpotDTO> listSpot(@RequestParam("cityid") Integer id) {
        return spotService.listSpotByCityId(id);
    }
    @GetMapping("/getspot")
    public SpotDTO getSpot(@RequestParam("id") Integer id) {
        return spotService.getSpotById(id);
    }
}
