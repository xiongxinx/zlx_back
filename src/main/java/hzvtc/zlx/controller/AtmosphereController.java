package hzvtc.zlx.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import hzvtc.zlx.dto.AtmosphereDTO;
import hzvtc.zlx.entity.AtmosphereDO;
import hzvtc.zlx.service.AtmosphereService;
import hzvtc.zlx.util.Constant;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/atmosphere")
@CrossOrigin
@RestController
public class AtmosphereController {
    private final AtmosphereService atmosphereService;

    public AtmosphereController(AtmosphereService atmosphereService) {
        this.atmosphereService = atmosphereService;
    }

    @GetMapping("/getlist")
    public Map<String, Object> gitList(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        Page<AtmosphereDTO> page1 = PageHelper.startPage(page, Constant.PAGE_SIZE);
        List<AtmosphereDTO> list = atmosphereService.listAll();
        Map<String, Object> map = new HashMap<>(16);
        map.put("pages", page1.getPages());
        map.put("list", list);
        return map;
    }

    @GetMapping("/list")
    public Map<String, Object> list(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        List<AtmosphereDTO> list = atmosphereService.list();
        Map<String, Object> map = new HashMap<>(16);
        map.put("list", list);
        return map;
    }

}
