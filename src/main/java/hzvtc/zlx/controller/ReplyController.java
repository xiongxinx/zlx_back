package hzvtc.zlx.controller;

import hzvtc.zlx.dto.ReplyDTO;
import hzvtc.zlx.entity.ReplyDO;
import hzvtc.zlx.service.ReplyService;
import org.springframework.web.bind.annotation.*;

/**
 * @author 熊新欣
 * @date 2020/8/6
 */
@RequestMapping("/reply")
@CrossOrigin
@RestController
public class ReplyController {
    private final ReplyService replyService;

    public ReplyController(ReplyService replyService) {
        this.replyService = replyService;
    }

    @PostMapping("/insert")
    public ReplyDTO insertReply(ReplyDO replyDO) {
        return replyService.insertReply(replyDO);
    }

    @GetMapping("/delete")
    public void deleteReply(@RequestParam("id") Integer id) {
        replyService.deleteReply(id);
    }
}
