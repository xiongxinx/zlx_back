package hzvtc.zlx.controller;

import hzvtc.zlx.entity.CityDO;
import hzvtc.zlx.service.CityService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/6
 */
@RequestMapping("city")
@CrossOrigin
@RestController
public class CityController {
    private final CityService cityService;

    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping("getcity")
    public CityDO getCityByName(@RequestParam("name") String name) {
        return cityService.getCityByName(name);
    }

    @GetMapping("/listcity")
    public List<CityDO> listCity(){
        return cityService.listCity();
    }
}
