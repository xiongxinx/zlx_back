package hzvtc.zlx.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import hzvtc.zlx.dto.ArticleDTO;
import hzvtc.zlx.dto.FoodDTO;
import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.entity.FoodDO;
import hzvtc.zlx.service.ArticleService;
import hzvtc.zlx.service.FoodService;
import hzvtc.zlx.util.Constant;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@RequestMapping("/food")
@CrossOrigin
@RestController
public class FoodController {
    private final FoodService foodService;

    public FoodController(FoodService foodService) {
        this.foodService = foodService;
    }

    @PostMapping("/insert")
    public Boolean insertFood(FoodDO food) {
        return foodService.insertFood(food);
    }

    @GetMapping("/getlist")
    public Map<String, Object> getList(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        Page<FoodDTO> page1 = PageHelper.startPage(page, Constant.PAGE_SIZE);
        List<FoodDTO> list = foodService.listAll();
        Map<String, Object> map = new HashMap<>(16);
        map.put("pages", page1.getPages());
        map.put("list",list);
        return map;
    }

    @GetMapping("/getfood")
    public FoodDTO getFoodById(@RequestParam("id") Integer id) {
        return foodService.getFoodById(id);
    }


}
