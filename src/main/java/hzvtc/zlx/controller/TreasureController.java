package hzvtc.zlx.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import hzvtc.zlx.dto.FoodDTO;
import hzvtc.zlx.dto.TreasureDTO;
import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.entity.FoodDO;
import hzvtc.zlx.entity.TreasureDO;
import hzvtc.zlx.service.FoodService;
import hzvtc.zlx.service.TreasureService;
import hzvtc.zlx.util.Constant;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@RequestMapping("/treasure")
@CrossOrigin
@RestController
public class TreasureController {
    private final TreasureService treasureService;

    public TreasureController(TreasureService treasureService) {
        this.treasureService = treasureService;
    }

    @PostMapping("/insert")
    public Boolean insertFood(TreasureDO treasureDO) {
        return  treasureService.insertTreasure(treasureDO);
    }

    @GetMapping("/getlist")
    public Map<String, Object> getList(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        Page<TreasureDTO> page1 = PageHelper.startPage(page, Constant.PAGE_SIZE);
        List<TreasureDTO> list = treasureService.listAll();
        Map<String, Object> map = new HashMap<>(16);
        map.put("pages", page1.getPages());
        map.put("list",list);
        return map;
    }

}
