package hzvtc.zlx.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import hzvtc.zlx.dto.HomeDTO;
import hzvtc.zlx.service.HomeService;
import hzvtc.zlx.util.Constant;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 熊新欣
 * @date 2020/8/14
 */
@CrossOrigin
@RestController
@RequestMapping("/home")
public class HomeController {
    private final HomeService homeService;

    public HomeController(HomeService homeService) {
        this.homeService = homeService;
    }
    @GetMapping("/list")
    public Map<String, Object> list(@RequestParam(value = "page",defaultValue = "1",required = false) Integer page) {
        Page<HomeDTO> page1 = PageHelper.startPage(page, Constant.PAGE_SIZE);
        List<HomeDTO> list = homeService.listHome();

        Map<String, Object> map = new HashMap<>(16);
        map.put("pages",page1.getPages());
        map.put("list", list);
        return map;
    }
}
