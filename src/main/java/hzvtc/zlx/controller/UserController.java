package hzvtc.zlx.controller;

import hzvtc.zlx.dto.UserDTO;
import hzvtc.zlx.entity.UserDO;
import hzvtc.zlx.service.UserService;
import hzvtc.zlx.util.Constant;
import hzvtc.zlx.util.FileSaveUtil;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@RequestMapping("/user")
@CrossOrigin
@RestController
public class UserController{
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public UserDTO checkLogin(@RequestParam(value = "username") String username, @RequestParam("password") String password) {
        UserDO userDO = new UserDO();
        if (username.contains("@")) {
            userDO.setEmail(username);
        } else {
            userDO.setPhoneNumber(username);
        }
        userDO.setPassword(password);
        return userService.doLogin(userDO);
    }

    @PostMapping("/register")
    public Boolean register(UserDO userDO, @RequestParam("username")String username) {
        if(username.contains("@")) {
            userDO.setEmail(username);
        } else {
            userDO.setPhoneNumber(username);
        }
        return userService.insertUser(userDO);
    }

    @GetMapping("/getuser")
    public UserDTO getUser(@RequestParam("id") Integer id){
        return userService.getUserById(id);
    }

    @GetMapping("/isvalid")
    public Boolean isValid(@RequestParam("username") String username) {
        UserDO userDO = new UserDO();
        if (username.contains("@")) {
            userDO.setEmail(username);
        } else {
            userDO.setPhoneNumber(username);
        }
        return userService.isValid(userDO);
    }

    @PostMapping(value = "/changebg")
    public String changeBackground(@RequestParam("bg") MultipartFile file,@RequestParam("id") Integer userId) {
        String fileName = FileSaveUtil.save(file);
        if (fileName == null) {
            return null;
        }
        UserDO userDO = new UserDO();
        userDO.setId(userId);
        userDO.setBackgroundPicture(fileName);
        userService.updateUserBackgroundPicture(userDO);
        return Constant.URL_PREFIX + fileName;
    }

    @PostMapping("/changeicon")
    public String changeIcon(@RequestParam("icon")MultipartFile file,@RequestParam("id") Integer userId) {
        String fileName = FileSaveUtil.save(file);
        if (fileName == null) {
            return null;
        }
        UserDO userDO = new UserDO();
        userDO.setId(userId);
        userDO.setIcon(fileName);
        userService.updateUserIcon(userDO);
        return Constant.URL_PREFIX + fileName;
    }
}
