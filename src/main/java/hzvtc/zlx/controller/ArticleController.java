package hzvtc.zlx.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import hzvtc.zlx.dto.ArticleDTO;
import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.service.ArticleService;
import hzvtc.zlx.util.Constant;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@RequestMapping("/article")
@CrossOrigin
@RestController
public class ArticleController {
    private final ArticleService articleService;

    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @PostMapping("/insert")
    public Boolean insertArticle(ArticleDO article) {
        return articleService.insertArticle(article);
    }

    @GetMapping("/getlist")
    public Map<String, Object> getList(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        Page<ArticleDTO> page1 = PageHelper.startPage(page, Constant.PAGE_SIZE);
        List<ArticleDTO> list = articleService.listAll();
        Map<String, Object> map = new HashMap<>(16);
        map.put("pages", page1.getPages());
        map.put("list",list);
        return map;
    }

    @GetMapping("/getarticle")
    public ArticleDTO getArticleById(@RequestParam("id") Integer id, @RequestParam("userid")Integer userId) {
        return articleService.getArticleById(id, userId);
    }

    @GetMapping("/like")
    public void like(@RequestParam("id") Integer id) {
        articleService.like(id);
    }

    @GetMapping("/dislike")
    public void disLike(@RequestParam("id") Integer id) {
        articleService.disLike(id);
    }

    @GetMapping("/listuser")
    public List<ArticleDTO> listArticleByUserId(@RequestParam("id") Integer id) {
        return articleService.listArticleByUserId(id);
    }
}
