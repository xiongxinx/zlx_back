package hzvtc.zlx.controller;

import hzvtc.zlx.dto.CommentDTO;
import hzvtc.zlx.entity.CommentDO;
import hzvtc.zlx.service.CommentService;
import org.springframework.web.bind.annotation.*;

/**
 * @author 熊新欣
 * @date 2020/8/6
 */
@RequestMapping("/comment")
@CrossOrigin
@RestController
public class CommentController {
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/insert")
    public CommentDTO insertComment(CommentDO commentDO) {
        return commentService.insertComment(commentDO);
    }

    @GetMapping("/delete")
    public void deleteComment(@RequestParam("id") Integer id) {
        commentService.deleteComment(id);
    }
}
