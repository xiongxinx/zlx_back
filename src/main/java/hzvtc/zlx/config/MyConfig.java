package hzvtc.zlx.config;

import hzvtc.zlx.util.Constant;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/**
 * @author 熊新欣
 * @date 2020/8/7
 */
@ComponentScan
@Configuration
public class MyConfig implements WebMvcConfigurer {
    /**
     * 配置访问静态资源路径
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("file:" + Constant.UPLOAD_PATH);
       /* registry.addResourceHandler("/treasure/**")
                .addResourceLocations("file:" + Constant.UPLOAD_PATH);
        registry.addResourceHandler("/city_culture/**")
                .addResourceLocations("file:" + Constant.UPLOAD_PATH);*/
        /*registry.addResourceHandler("/city_culture/**")
                .addResourceLocations("file:" + Constant.UPLOAD_PATH);*/

    }
}
