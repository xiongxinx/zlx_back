package hzvtc.zlx.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author 熊新欣
 * @date 2020/8/4
 * 寻画实体类
 */
@Data
public class PhotoDO {
    private Integer id;
    private String images;
    private String content;
    private Integer postUser;
    private Date date;
}
