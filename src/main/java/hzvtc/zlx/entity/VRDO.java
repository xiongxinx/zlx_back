package hzvtc.zlx.entity;

import lombok.Data;

/**
 * 美食实体类
 * @author 求琰锋
 * @date 2020/8/24
 */
@Data
public class VRDO {
    private Integer id;
    private String vrName;
    private String content;
    private String url;
    private String img;
}
