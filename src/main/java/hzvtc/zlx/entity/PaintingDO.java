package hzvtc.zlx.entity;

import lombok.Data;

@Data
public class PaintingDO {
    private Integer id;
    private String title;
    private String content;
    private String img;
}
