package hzvtc.zlx.entity;

import lombok.Data;

/**
 * @author 熊新欣
 * @date 2020/8/4
 * 城市实体类
 */
@Data
public class CityDO {
    private Integer id;
    private String name;
    private String icon;
    private String backgroundPicture;
}
