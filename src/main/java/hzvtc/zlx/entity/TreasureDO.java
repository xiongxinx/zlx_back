package hzvtc.zlx.entity;

import lombok.Data;

import java.util.Date;

/**
 * 文章实体类
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class TreasureDO {
    private Integer id;
    private String treasureName;
    private String place;
    private String content;
    private String img;
    private Integer classifyId;
}
