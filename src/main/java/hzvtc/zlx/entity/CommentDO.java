package hzvtc.zlx.entity;

import lombok.Data;

import java.util.Date;

/**
 * 评论实体类
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class CommentDO {
    private Integer id;
    private String content;
    private Integer modular;
    private Integer targetId;
    private Date time;
    private Integer userId;
}
