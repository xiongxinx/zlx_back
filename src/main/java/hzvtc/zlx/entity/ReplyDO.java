package hzvtc.zlx.entity;

import lombok.Data;

import java.util.Date;

/**
 * 回复实体类
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class ReplyDO {
    private Integer id;
    private Integer userId;
    private Integer commentId;
    private String content;
    private Date time;
    private Integer replyToId;
}
