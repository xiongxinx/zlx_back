package hzvtc.zlx.entity;

import lombok.Data;

/**
 * 景点实体类
 * @author 熊新欣
 * @date 2020/8/12
 */
@Data
public class SpotDO {
    private Integer id;
    private String title;
    private String images;
    private String address;
    private Float score;
    private Integer commentCount;
    private String tags;
    private Float longitude;
    private Float latitude;
    private String openTime;
    private Integer belongCity;
    private String introduction;
    private String cost;
    private String grade;
}
