package hzvtc.zlx.entity;

import lombok.Data;

import java.util.Date;

/**
 * 文章实体类
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class ArticleDO {
    private Integer id;
    private String title;
    private String content;
    private Integer author;
    private String coverPicture;
    private Date date;
    private Integer likeNumber;
}
