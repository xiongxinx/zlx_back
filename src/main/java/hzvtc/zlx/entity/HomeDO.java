package hzvtc.zlx.entity;

import lombok.Data;

/**
 * @author 熊新欣
 * @date 2020/8/14
 */
@Data
public class HomeDO {
    private Integer id;
    private Integer targetId;
    private Integer typeId;
    private String typeName;
    private String url;
}
