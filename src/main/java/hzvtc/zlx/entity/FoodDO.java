package hzvtc.zlx.entity;

import lombok.Data;

/**
 * 美食实体类
 * @author 求琰锋
 * @date 2020/8/24
 */
@Data
public class FoodDO {
    private Integer id;
    private String foodName;
    private String introduce;
    private String pic;
    private Integer city;
    private String detail;
}
