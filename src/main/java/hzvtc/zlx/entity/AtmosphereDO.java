package hzvtc.zlx.entity;

import lombok.Data;

import java.util.Date;

@Data
public class AtmosphereDO {
    private Integer id;
    private String cityCultureName;
    private String describe;
    private CityDO city;
//    private SightDo sight;
    private String businessHours;
    private Date addTime;
    private String images;
}
