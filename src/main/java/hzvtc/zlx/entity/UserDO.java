package hzvtc.zlx.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author 熊新欣
 * @date 2020/8/4
 * 用户实体类
 */
@Data
public class UserDO {
    private Integer id;
    private String name;
    private String icon;
    private String backgroundPicture;
    private String email;
    private String phoneNumber;
    private String password;
    private Date createTime;
    private Integer role;
}
