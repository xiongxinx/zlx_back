package hzvtc.zlx.entity;

import lombok.Data;

/**
 * 收藏实体类
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class CollectionDO {
    private Integer id;
    private Integer userId;
    private Integer articleId;
}
