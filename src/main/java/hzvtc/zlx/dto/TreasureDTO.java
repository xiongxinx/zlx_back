package hzvtc.zlx.dto;

import lombok.Data;

import java.util.List;

/**
 * 文章实体类
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class TreasureDTO {
    private Integer id;
    private String treasureName;
    private String place;
    private String content;
    private List<String> img;
    private Integer classifyId;
}
