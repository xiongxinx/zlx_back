package hzvtc.zlx.dto;


import lombok.Data;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class CommentDTO {
    private Integer id;
    private String content;
    private String time;
    private UserDTO user;
    private List<ReplyDTO> replys;
}
