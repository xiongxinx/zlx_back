package hzvtc.zlx.dto;

import lombok.Data;

import java.util.List;

/**
 * @author 求琰锋
 * @date 2020/8/24
 */
@Data
public class VRDTO {
    private Integer id;
    private String vrName;
    private String content;
    private String url;
    private String img;
}
