package hzvtc.zlx.dto;

import hzvtc.zlx.entity.CityDO;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class AtmosphereDTO {
    private Integer id;
    private String cityCultureName;
    private String describe;
    private CityDO city;
    //    private SightDo sight;
    private String businessHours;
    private String time;
    private List<String> images;
}
