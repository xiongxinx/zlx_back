package hzvtc.zlx.dto;


import lombok.Data;

import java.util.Date;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class ReplyDTO {
    private Integer id;
    private UserDTO user;
    private String content;
    private String time;
    private UserDTO replyTo;
    private CommentDTO commentDTO;
}
