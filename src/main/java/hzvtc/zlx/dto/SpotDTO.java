package hzvtc.zlx.dto;

import lombok.Data;

import java.util.List;
/**
 * 景点数据传输对象
 * @author 熊新欣
 * @date 2020/8/12
 */
@Data
public class SpotDTO {
    private Integer id;
    private String title;
    private List<String> images;
    private String address;
    private Float score;
    private Integer commentCount;
    private String tags;
    private String openTime;
    private Float longitude;
    private Float latitude;
    private String introduction;
    private String cost;
    private String grade;
}
