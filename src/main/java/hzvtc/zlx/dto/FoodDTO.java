package hzvtc.zlx.dto;

import lombok.Data;

import java.util.List;

/**
 * @author 求琰锋
 * @date 2020/8/24
 */
@Data
public class FoodDTO {
    private Integer id;
    private String foodName;
    private String introduce;
    private List<String> pic;
    private Integer city;
    private String detail;
}
