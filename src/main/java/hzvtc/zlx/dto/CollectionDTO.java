package hzvtc.zlx.dto;

import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.entity.UserDO;
import lombok.Data;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class CollectionDTO {
    private ArticleDO article;
}
