package hzvtc.zlx.dto;


import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class ArticleDTO {
    private Integer id;
    private String title;
    private String content;
    private UserDTO author;
    private String coverPicture;
    private String date;
    private Integer likeNumber;
    private Integer saveNumber;
    private Boolean saved;
    private List<CommentDTO> comments;
}
