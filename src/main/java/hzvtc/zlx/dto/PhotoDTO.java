package hzvtc.zlx.dto;

import lombok.Data;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class PhotoDTO {
    private Integer id;
    private List<String> images;
    private String content;
    private UserDTO postUser;
    private String date;
    List<CommentDTO> comment;
}
