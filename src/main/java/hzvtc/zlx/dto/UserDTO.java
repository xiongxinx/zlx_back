package hzvtc.zlx.dto;

import lombok.Data;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Data
public class UserDTO {
    private Integer id;
    private String name;
    private String icon;
    private String backgroundPicture;
    private Integer role;
}
