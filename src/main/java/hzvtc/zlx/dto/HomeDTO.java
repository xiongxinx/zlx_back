package hzvtc.zlx.dto;

import lombok.Data;

/**
 * @author 熊新欣
 * @date 2020/8/14
 */
@Data
public class HomeDTO {
    private String image;
    private String title;
    private String content;
    private UserDTO user;
    private String type;
    private String url;
}
