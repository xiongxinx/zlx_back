package hzvtc.zlx.dto;

import lombok.Data;

import java.util.List;

@Data
public class CityDTO {
    private Integer id;
    private String name;
    private List<String> icon;
    private List<String> backgroundPicture;
}
