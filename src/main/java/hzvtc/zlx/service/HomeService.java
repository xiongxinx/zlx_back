package hzvtc.zlx.service;

import hzvtc.zlx.dao.*;
import hzvtc.zlx.dto.HomeDTO;
import hzvtc.zlx.entity.HomeDO;
import hzvtc.zlx.util.HomeConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/14
 */
@Service
public class HomeService {
    private HomeMapper homeMapper;
    private ArticleMapper articleMapper;
    private PhotoMapper photoMapper;
    private SpotMapper spotMapper;
    private FoodMapper foodMapper;
    private final UserService userService;

    public HomeService(HomeMapper homeMapper,
                       ArticleMapper articleMapper,
                       PhotoMapper photoMapper,
                       SpotMapper spotMapper, FoodMapper foodMapper, UserService userService) {
        this.homeMapper = homeMapper;
        this.articleMapper = articleMapper;
        this.photoMapper = photoMapper;
        this.spotMapper = spotMapper;
        this.foodMapper = foodMapper;
        this.userService = userService;
    }

    public List<HomeDTO> listHome() {
        List<HomeDO> homeDOList = homeMapper.listHome();
        List<HomeDTO> homeDTOList = new ArrayList<>(16);
        HomeConvertUtil convert = new HomeConvertUtil(articleMapper,
                photoMapper, spotMapper, foodMapper, userService);
        homeDOList.forEach(homeDO -> homeDTOList.add(convert.getDTO(homeDO)));
        return homeDTOList;
    }
}
