package hzvtc.zlx.service;

import hzvtc.zlx.dao.ArticleMapper;
import hzvtc.zlx.dto.ArticleDTO;
import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.util.ArticleConvertUtil;
import hzvtc.zlx.util.DateFormatUtil;
import hzvtc.zlx.util.RemoveTagsUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Service
public class ArticleService {
    private final ArticleMapper articleMapper;
    private final UserService userService;
    private final CommentService commentService;
    private final CollectionService collectionService;

    public ArticleService(ArticleMapper articleMapper, UserService userService, CommentService commentService, CollectionService collectionService) {
        this.articleMapper = articleMapper;
        this.userService = userService;
        this.commentService = commentService;
        this.collectionService = collectionService;
    }

    public Boolean insertArticle(ArticleDO article) {
        Integer insertNumber = articleMapper.insertArticle(article);
        return insertNumber != null && insertNumber > 0;
    }

    public List<ArticleDTO> listAll() {
        List<ArticleDO> articleDOList = articleMapper.listArticle();
        List<ArticleDTO> result = new ArrayList<>();
        ArticleConvertUtil articleConvertUtil = new ArticleConvertUtil(userService);
        articleDOList.forEach(articleDO -> result.add(articleConvertUtil.convert(articleDO)));
        return result;
    }

    public ArticleDTO getArticleById(Integer id, Integer userId){
        ArticleDO articleDO = articleMapper.getArticleById(id);
        ArticleConvertUtil articleConvertUtil = new ArticleConvertUtil(userService, commentService, collectionService);
        ArticleDTO articleDTO = articleConvertUtil.convert(articleDO);
        if (userId != 0) {
            articleDTO.setSaved(collectionService.saved(id, userId));
        } else {
            articleDTO.setSaved(false);
        }
        return articleDTO;
    }

    public void like(Integer id) {
        articleMapper.updateLikeNumberAdd(id);
    }

    public void disLike(Integer id) {
        articleMapper.updateLikeNumberSub(id);
    }

    public List<ArticleDTO> listArticleByUserId(Integer id) {
        List<ArticleDO> articleDOList = articleMapper.listArticleByUserId(id);
        List<ArticleDTO> articleDTOList = new ArrayList<>(16);
        articleDOList.forEach(articleDO ->{
            ArticleConvertUtil convertUtil = new ArticleConvertUtil(userService);
            articleDTOList.add(convertUtil.convert(articleDO));
        });
        return articleDTOList;
    }
}
