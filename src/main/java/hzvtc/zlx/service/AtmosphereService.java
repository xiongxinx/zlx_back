package hzvtc.zlx.service;

import com.alibaba.fastjson.JSON;
import hzvtc.zlx.dao.AtmosphereMapper;
import hzvtc.zlx.dto.AtmosphereDTO;
import hzvtc.zlx.entity.AtmosphereDO;
import hzvtc.zlx.util.AtmosphereConvertUtil;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static hzvtc.zlx.util.Constant.UPLOAD_PATH;

@Service
public class AtmosphereService {
    private final AtmosphereMapper atmosphereMapper;

    public AtmosphereService(AtmosphereMapper atmosphereMapper){
        this.atmosphereMapper = atmosphereMapper;
    }

    public List<AtmosphereDTO> listAll() {
        List<AtmosphereDO> atmosphereList = atmosphereMapper.listAtmosphere();
        List<AtmosphereDTO> result = new ArrayList<>();

        AtmosphereConvertUtil atmosphereConvertUtil = new AtmosphereConvertUtil();
        atmosphereList.forEach(atmosphereDO -> result.add(atmosphereConvertUtil.convert(atmosphereDO)));

        return result;
    }

    public List<AtmosphereDTO> list() {

        List<AtmosphereDTO> atmosphereList = null;
        try {
//            String filePathh = UPLOAD_PATH + "data2.json";//json文件地址
            String filePathh = "E:\\upload\\data2.json";//json文件地址
            InputStream inputStream = new FileInputStream(filePathh);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line=br.readLine())!=null){
                sb.append(line);
            }
            atmosphereList = JSON.parseArray(sb.toString(), AtmosphereDTO.class);

        } catch (IOException e) {
            e.printStackTrace();
        }
//        for (AtmosphereDTO item : atmosphereList) {
//            System.out.println(item.getTime());
//        }
       /* for (AtmosphereDTO item : atmosphereList) {
            AtmosphereDTO atmosphereDTO = new AtmosphereDTO();
            List<String> imagelist = item.getImages();
            System.out.println(imagelist);
            atmosphereDTO.setImages(imagelist);
            atmosphereDTO.setCityCultureName(item.getCityCultureName());
            atmosphereDTO.setDescribe(item.getDescribe());
            atmosphereDTO.setId(item.getId());
        }*/

        return atmosphereList;
    }
}
