package hzvtc.zlx.service;

import hzvtc.zlx.dao.ReplyMapper;
import hzvtc.zlx.dto.ReplyDTO;
import hzvtc.zlx.entity.ReplyDO;
import hzvtc.zlx.util.DateFormatUtil;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/6
 */
@Service
public class ReplyService {
    private final ReplyMapper replyMapper;
    private final UserService userService;

    public ReplyService(ReplyMapper replyMapper, UserService userService) {
        this.replyMapper = replyMapper;
        this.userService = userService;
    }

    public List<ReplyDTO> listReplyByCommentId(Integer commentId){
        List<ReplyDO> replyDOList = replyMapper.listReplyByCommentId(commentId);
        List<ReplyDTO> replyDTOList = new ArrayList<>(16);
        replyDOList.forEach(replyDO -> {

            replyDTOList.add(replyConvert(replyDO));
        });
        return replyDTOList;
    }

    public ReplyDTO insertReply(ReplyDO replyDO) {
        replyMapper.insertReply(replyDO);
        return replyConvert(replyMapper.getReplyById(replyDO.getId()));
    }

    private ReplyDTO replyConvert(ReplyDO replyDO) {
        ReplyDTO replyDTO = new ReplyDTO();
        replyDTO.setId(replyDO.getId());
        replyDTO.setContent(replyDO.getContent());
        replyDTO.setUser(userService.getUserById(replyDO.getUserId()));
        replyDTO.setTime(DateFormatUtil.dateFormat(replyDO.getTime()));
        replyDTO.setReplyTo(userService.getUserById(replyDO.getReplyToId()));
        return replyDTO;
    }

    public void deleteReply(Integer id) {
        replyMapper.deleteReply(id);
    }
}
