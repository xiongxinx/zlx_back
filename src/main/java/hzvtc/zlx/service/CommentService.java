package hzvtc.zlx.service;

import hzvtc.zlx.dao.CommentMapper;
import hzvtc.zlx.dto.CommentDTO;
import hzvtc.zlx.entity.CommentDO;
import hzvtc.zlx.util.DateFormatUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
/**
 * @author 熊新欣
 * @date 2020/8/6
 */
@Service
public class CommentService {
    private final CommentMapper commentMapper;
    private final UserService userService;
    private final ReplyService replyService;

    public CommentService(CommentMapper commentMapper, UserService userService, ReplyService replyService) {
        this.commentMapper = commentMapper;
        this.userService = userService;
        this.replyService = replyService;
    }

    public List<CommentDTO> getCommentsByTargetIdAndModular(Integer targetId, Integer modular){
        List<CommentDO> listCommentDO = commentMapper.getCommentsByTargetIdAndModular(targetId, modular);
        List<CommentDTO> listCommentDTO = new ArrayList<>(16);
        listCommentDO.forEach(commentDO -> listCommentDTO.add(commentConvert(commentDO)));
        return listCommentDTO;
    }

    public CommentDTO insertComment(CommentDO commentDO) {
        commentMapper.insertComment(commentDO);
        return commentConvert(commentMapper.getCommentById(commentDO.getId()));
    }

    private CommentDTO commentConvert(CommentDO commentDO) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(commentDO.getId());
        commentDTO.setContent(commentDO.getContent());
        commentDTO.setTime(DateFormatUtil.dateFormat(commentDO.getTime()));
        commentDTO.setUser(userService.getUserById(commentDO.getUserId()));
        commentDTO.setReplys(replyService.listReplyByCommentId(commentDO.getId()));
        return commentDTO;
    }

    public void deleteComment(Integer id) {
        commentMapper.deleteComment(id);
    }
}
