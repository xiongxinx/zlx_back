package hzvtc.zlx.service;

import hzvtc.zlx.dao.CityMapper;
import hzvtc.zlx.entity.CityDO;
import hzvtc.zlx.util.Constant;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/6
 */
@Service
public class CityService {
    private final CityMapper cityMapper;

    public CityService(CityMapper cityMapper) {
        this.cityMapper = cityMapper;
    }

    public CityDO getCityByName(String name) {
        CityDO cityDO = cityMapper.getCityByName(name);
        cityDO.setIcon(Constant.URL_PREFIX + cityDO.getIcon());
        cityDO.setBackgroundPicture(Constant.URL_PREFIX + cityDO.getBackgroundPicture());
        return cityDO;
    }

    public List<CityDO> listCity() {
        List<CityDO> cityDOList = cityMapper.listCity();
        cityDOList.forEach(cityDO -> {
            cityDO.setBackgroundPicture(Constant.URL_PREFIX + cityDO.getBackgroundPicture());
            cityDO.setIcon(Constant.URL_PREFIX + cityDO.getIcon());
        });
        return cityDOList;
    }
}
