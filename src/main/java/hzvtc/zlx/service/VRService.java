package hzvtc.zlx.service;

import hzvtc.zlx.dao.VRMapper;
import hzvtc.zlx.dto.AtmosphereDTO;
import hzvtc.zlx.dto.VRDTO;
import hzvtc.zlx.entity.AtmosphereDO;
import hzvtc.zlx.entity.VRDO;
import hzvtc.zlx.util.AtmosphereConvertUtil;
import hzvtc.zlx.util.VRConvertUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 求琰锋
 * @date 2020/8/24
 */
@Service
public class VRService {
    private final VRMapper vrMapper;
    private final UserService userService;
    private final CommentService commentService;
    private final CollectionService collectionService;

    public VRService(VRMapper vrMapper, UserService userService, CommentService commentService, CollectionService collectionService) {
        this.vrMapper = vrMapper;
        this.userService = userService;
        this.commentService = commentService;
        this.collectionService = collectionService;
    }

    public Boolean insertVR(VRDO vrdo) {
        Integer insertNumber = vrMapper.insertVR(vrdo);
        return insertNumber != null && insertNumber > 0;
    }

    public List<VRDTO> listAll() {
        List<VRDO> VRDOList = vrMapper.listVR();
        List<VRDTO> result = new ArrayList<>();
        VRConvertUtil vrConvertUtil = new VRConvertUtil();
        VRDOList.forEach(vrdo -> result.add(vrConvertUtil.convert(vrdo)));
        return result;
    }

}
