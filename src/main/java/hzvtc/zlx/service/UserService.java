package hzvtc.zlx.service;


import hzvtc.zlx.dao.UserMapper;
import hzvtc.zlx.dto.UserDTO;
import hzvtc.zlx.entity.UserDO;
import hzvtc.zlx.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Service
public class UserService {
    private final UserMapper userMapper;

    public UserService(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public UserDTO doLogin(UserDO user){
        UserDO userDO = userMapper.doLogin(user);
        return userConvert(userDO);
    }

    public Boolean insertUser(UserDO user) {
        Integer insertNumber = userMapper.insertUser(user);
        return insertNumber != null && insertNumber > 0;
    }

    public UserDTO getUserById(Integer id) {
        UserDO userDO = userMapper.getUserById(id);
        return userConvert(userDO);
    }

    public void updateUserIcon(UserDO userDO) {
        userMapper.updateUserIcon(userDO);
    }

    public void updateUserBackgroundPicture(UserDO userDO) {
        userMapper.updateUserBackgroundPicture(userDO);
    }

    public Boolean isValid(UserDO userDO) {
        UserDO userFromDateBase = userMapper.isValid(userDO);
        return userFromDateBase == null;
    }
    private UserDTO userConvert(UserDO userDO) {
        if (userDO == null) {
            return null;
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userDO.getId());
        userDTO.setName(userDO.getName());
        userDTO.setBackgroundPicture(Constant.URL_PREFIX + userDO.getBackgroundPicture());
        userDTO.setIcon(Constant.URL_PREFIX + userDO.getIcon());
        userDTO.setRole(userDO.getRole());
        return userDTO;
    }
}
