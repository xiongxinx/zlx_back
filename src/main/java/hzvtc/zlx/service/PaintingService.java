package hzvtc.zlx.service;

import hzvtc.zlx.dao.PaintingMapper;
import hzvtc.zlx.entity.PaintingDO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaintingService {
    private final PaintingMapper paintingMapper;


    public PaintingService(PaintingMapper paintingMapper) {
        this.paintingMapper = paintingMapper;
    }

    public List<PaintingDO> listAll() {
        List<PaintingDO> paintingList = paintingMapper.listPainting();

        return paintingList;
    }
}
