package hzvtc.zlx.service;

import hzvtc.zlx.dao.ArticleMapper;
import hzvtc.zlx.dao.FoodMapper;
import hzvtc.zlx.dto.ArticleDTO;
import hzvtc.zlx.dto.FoodDTO;
import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.entity.FoodDO;
import hzvtc.zlx.util.ArticleConvertUtil;
import hzvtc.zlx.util.FoodConvertUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 求琰锋
 * @date 2020/8/24
 */
@Service
public class FoodService {
    private final FoodMapper foodMapper;
    private final UserService userService;
    private final CommentService commentService;
    private final CollectionService collectionService;

    public FoodService(FoodMapper foodMapper, UserService userService, CommentService commentService, CollectionService collectionService) {
        this.foodMapper = foodMapper;
        this.userService = userService;
        this.commentService = commentService;
        this.collectionService = collectionService;
    }

    public Boolean insertFood(FoodDO food) {
        Integer insertNumber = foodMapper.insertFood(food);
        return insertNumber != null && insertNumber > 0;
    }

    public List<FoodDTO> listAll() {
        List<FoodDO> foodDOList = foodMapper.listFoods();
        List<FoodDTO> result = new ArrayList<>();
        FoodConvertUtil foodConvertUtil = new FoodConvertUtil(userService);
        foodDOList.forEach(foodDO -> result.add(foodConvertUtil.convert(foodDO)));
        return result;
    }
    public FoodDTO getFoodById(Integer id){
        FoodDO foodDO = foodMapper.getFoodById(id);
        FoodConvertUtil foodConvertUtil = new FoodConvertUtil(userService, commentService, collectionService);
        FoodDTO foodDTO = foodConvertUtil.convert(foodDO);

        return foodDTO;
    }
}
