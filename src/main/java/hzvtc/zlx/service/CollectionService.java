package hzvtc.zlx.service;

import hzvtc.zlx.dao.ArticleMapper;
import hzvtc.zlx.dao.CollectionMapper;
import hzvtc.zlx.dto.ArticleDTO;
import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.entity.CollectionDO;
import hzvtc.zlx.util.ArticleConvertUtil;
import hzvtc.zlx.util.RemoveTagsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/7
 */
@Service
public class CollectionService {
    private final CollectionMapper collectionMapper;
    private final ArticleMapper articleMapper;
    private final UserService userService;
    public CollectionService(CollectionMapper collectionMapper, ArticleMapper articleMapper, UserService userService) {
        this.collectionMapper = collectionMapper;
        this.articleMapper = articleMapper;
        this.userService = userService;
    }

    public Integer countCollectionNumber(Integer id){
        return collectionMapper.countCollectionByArticleId(id);
    }

    public List<ArticleDTO> listCollectionByUserId(Integer id) {
        List<CollectionDO> collectionDOList = collectionMapper.listCollectionByUserId(id);
        List<ArticleDTO> articleDTOList = new ArrayList<>(16);
        collectionDOList.forEach(collectionDO -> {
            ArticleDO articleDO = articleMapper.getArticleById(collectionDO.getArticleId());
            ArticleConvertUtil convertUtil = new ArticleConvertUtil(userService);
            articleDTOList.add(convertUtil.convert(articleDO));
        });
        return articleDTOList;
    }

    public void deleteCollection(CollectionDO collectionDO) {
        collectionMapper.deleteCollectionByArticleIdAndUserId(collectionDO);
    }

    public void insertCollection(CollectionDO collectionDO) {
        collectionMapper.insertCollection(collectionDO);
    }

    public Boolean saved(Integer articleId, Integer userId) {
        CollectionDO collectionDO = collectionMapper.saved(articleId, userId);
        return collectionDO != null;
    }
}
