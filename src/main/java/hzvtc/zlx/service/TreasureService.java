package hzvtc.zlx.service;

import hzvtc.zlx.dao.TreasureMapper;
import hzvtc.zlx.dao.VRMapper;
import hzvtc.zlx.dto.ArticleDTO;
import hzvtc.zlx.dto.TreasureDTO;
import hzvtc.zlx.entity.ArticleDO;
import hzvtc.zlx.entity.TreasureDO;
import hzvtc.zlx.entity.VRDO;
import hzvtc.zlx.util.ArticleConvertUtil;
import hzvtc.zlx.util.TreasureConvertUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 求琰锋
 * @date 2020/8/24
 */
@Service
public class TreasureService {
    private final TreasureMapper treasureMapper;
    private final UserService userService;
    private final CommentService commentService;
    private final CollectionService collectionService;

    public TreasureService(TreasureMapper treasureMapper, UserService userService, CommentService commentService, CollectionService collectionService) {
        this.treasureMapper = treasureMapper;
        this.userService = userService;
        this.commentService = commentService;
        this.collectionService = collectionService;
    }

    public Boolean insertTreasure(TreasureDO treasureDO) {
        Integer insertNumber = treasureMapper.insertTreasure(treasureDO);
        return insertNumber != null && insertNumber > 0;
    }

    public List<TreasureDTO> listAll() {
        List<TreasureDO> treasureDOList = treasureMapper.listTreasure();
        List<TreasureDTO> result = new ArrayList<>();
        TreasureConvertUtil treasureConvertUtil = new TreasureConvertUtil(userService);
        treasureDOList.forEach(treasureDO -> result.add(treasureConvertUtil.convert(treasureDO)));
        return result;
    }

}
