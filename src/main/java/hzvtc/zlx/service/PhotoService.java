package hzvtc.zlx.service;

import hzvtc.zlx.dao.PhotoMapper;
import hzvtc.zlx.dto.PhotoDTO;
import hzvtc.zlx.entity.PhotoDO;
import hzvtc.zlx.util.Constant;
import hzvtc.zlx.util.DateFormatUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 熊新欣
 * @date 2020/8/7
 */
@Service
public class PhotoService {
    private final PhotoMapper photoMapper;
    private final UserService userService;
    private final CommentService commentService;
    public PhotoService(PhotoMapper photoMapper, UserService userService, CommentService commentService) {
        this.photoMapper = photoMapper;
        this.userService = userService;
        this.commentService = commentService;
    }

    public List<PhotoDTO> listPhoto() {
        List<PhotoDO> photoDOList = photoMapper.listPhoto();
        List<PhotoDTO> photoDTOList = new ArrayList<>(16);
        photoDOList.forEach(photoDO -> photoDTOList.add(photoConvert(photoDO)));
        return photoDTOList;
    }

    public void insertPhoto(PhotoDO photoDO) {
        photoMapper.insertPhoto(photoDO);
    }

    public void deletePhoto(Integer id) {
        photoMapper.deletePhoto(id);
    }

    public PhotoDTO getPhotoById(Integer id) {
        PhotoDO photoDO = photoMapper.getPhotoById(id);
        PhotoDTO photoDTO = photoConvert(photoDO);
        photoDTO.setComment(commentService.getCommentsByTargetIdAndModular(photoDO.getId(),
                Constant.PHOTO_MODULAR_NUMBER));
        return photoDTO;
    }

    private PhotoDTO photoConvert(PhotoDO photoDO) {
        PhotoDTO photoDTO = new PhotoDTO();
        photoDTO.setContent(photoDO.getContent());
        photoDTO.setDate(DateFormatUtil.dateFormat(photoDO.getDate()));
        photoDTO.setId(photoDO.getId());
        List<String> images = Arrays.stream(photoDO.getImages()
                .split(","))
                .map(s -> Constant.URL_PREFIX + s).collect(Collectors.toList());
        photoDTO.setImages(images);
        photoDTO.setPostUser(userService.getUserById(photoDO.getPostUser()));
        return photoDTO;
    }
}
