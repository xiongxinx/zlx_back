package hzvtc.zlx.service;

import hzvtc.zlx.dao.SpotMapper;
import hzvtc.zlx.dto.SpotDTO;
import hzvtc.zlx.entity.SpotDO;
import hzvtc.zlx.util.Constant;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 熊新欣
 * @date 2020/8/12
 */
@Service
public class SpotService {
    private final SpotMapper spotMapper;

    public SpotService(SpotMapper spotMapper) {
        this.spotMapper = spotMapper;
    }

    public List<SpotDTO> listSpotByCityId(Integer id) {
        List<SpotDO> spotDOList = spotMapper.getSpotByCityId(id);
        List<SpotDTO> spotDTOList = new ArrayList<>(16);
        spotDOList.forEach(spotDO -> spotDTOList.add(spotConvert(spotDO)));
        return spotDTOList;
    }

    public SpotDTO getSpotById(Integer id) {
        SpotDO spotDO = spotMapper.getSpotById(id);
        return spotConvert(spotDO);
    }

    private SpotDTO spotConvert(SpotDO spotDO) {
        SpotDTO spotDTO = new SpotDTO();
        spotDTO.setId(spotDO.getId());
        spotDTO.setTitle(spotDO.getTitle());
        List<String> images = Arrays.stream(spotDO.getImages().split(","))
                .map(s -> Constant.URL_PREFIX + s).collect(Collectors.toList());
        spotDTO.setImages(images);
        spotDTO.setAddress(spotDO.getAddress());
        spotDTO.setScore(spotDO.getScore());
        spotDTO.setCommentCount(spotDO.getCommentCount());
        spotDTO.setTags(spotDO.getTags());
        spotDTO.setOpenTime(spotDO.getOpenTime());
        spotDTO.setLongitude(spotDO.getLongitude());
        spotDTO.setLatitude(spotDO.getLatitude());
        spotDTO.setIntroduction(spotDO.getIntroduction());
        spotDTO.setCost(spotDO.getCost());
        spotDTO.setGrade(spotDO.getGrade());
        return spotDTO;
    }
}
