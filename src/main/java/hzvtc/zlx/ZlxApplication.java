package hzvtc.zlx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@SpringBootApplication
@MapperScan("hzvtc.zlx.dao")
public class ZlxApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZlxApplication.class, args);
    }

}
