package hzvtc.zlx.dao;

import hzvtc.zlx.entity.TreasureDO;
import hzvtc.zlx.entity.VRDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 求琰锋
 * @date 2020/8/24
 */
@Repository
public interface TreasureMapper {
    /**
     * 插入treasure
     * @return 插入的数量
     * @param treasureDO treasure
     */
    Integer insertTreasure(TreasureDO treasureDO);

    /**
     * 获取文章列表
     * @return 文章列表
     */
    List<TreasureDO> listTreasure();

}
