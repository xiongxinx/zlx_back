package hzvtc.zlx.dao;


import hzvtc.zlx.entity.CommentDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Repository
public interface CommentMapper {
    /**
     * 根据目标id和模块获取评论
     * @param targetId 目标id
     * @param modular 模块
     * @return 评论列表
     */
    List<CommentDO> getCommentsByTargetIdAndModular(@Param("targetId") Integer targetId,
                                                    @Param("modular") Integer modular);

    /**
     * 添加评论
     * @param commentDO 插入的评论
     */
    void insertComment(CommentDO commentDO);

    /**
     * 删除评论
     * @param id 删除评论的id
     */
    void deleteComment(Integer id);

    /**
     * 根据id获取评论
     * @return 评论
     * @param id 评论id
     */
    CommentDO getCommentById(Integer id);

    /**
     * 根据用户id 获取评论列表
     * @param id 用户id
     * @return 评论列表
     */
    List<CommentDO> listCommentByUserId(Integer id);
}
