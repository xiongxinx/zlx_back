package hzvtc.zlx.dao;

import hzvtc.zlx.entity.FoodDO;
import hzvtc.zlx.entity.VRDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 求琰锋
 * @date 2020/8/24
 */
@Repository
public interface VRMapper {
    /**
     * 插入vr
     * @return 插入的数量
     * @param vrdo vr
     */
    Integer insertVR(VRDO vrdo);

    /**
     * 获取文章列表
     * @return 文章列表
     */
    List<VRDO> listVR();

}
