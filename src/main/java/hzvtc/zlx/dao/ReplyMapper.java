package hzvtc.zlx.dao;


import hzvtc.zlx.entity.ReplyDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Repository
public interface ReplyMapper {
    /**
     * 根据 评论id获取回复列表
     * @return 回复列表
     * @param commentId 评论id
     */
    List<ReplyDO> listReplyByCommentId(Integer commentId);
    /**
     * 添加回复
     * @param replyDO 插入的评论
     */
    void insertReply(ReplyDO replyDO);
    /**
     * 删除回复
     * @param id 删除评论的id
     */
    void deleteReply(Integer id);

    /**
     * 根据id获取回复
     * @return 回复
     * @param id 回复id
     */
    ReplyDO getReplyById(Integer id);
}
