package hzvtc.zlx.dao;

import hzvtc.zlx.entity.PaintingDO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaintingMapper {
    /**
     * 获取画列表
     * @return 画列表
     */
    List<PaintingDO> listPainting();
}
