package hzvtc.zlx.dao;

import hzvtc.zlx.entity.HomeDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/14
 */
@Repository
public interface HomeMapper {
    /**
     * 获取首页推荐列表
     * @return ...
     */
    List<HomeDO> listHome();
}
