package hzvtc.zlx.dao;


import hzvtc.zlx.entity.PhotoDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/7
 */
@Repository
public interface PhotoMapper {
    /**
     * 寻画列表
     * @return 图片列表
     */
    List<PhotoDO> listPhoto();

    /**
     * 获取寻画详情
     * @param id id
     * @return 详情
     */
    PhotoDO getPhotoById(Integer id);

    /**
     * 插入
     * @param photoDO 。
     */
    void insertPhoto(PhotoDO photoDO);

    /**
     * 删除
     * @param id id
     */
    void deletePhoto(Integer id);
}
