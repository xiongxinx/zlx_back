package hzvtc.zlx.dao;

import hzvtc.zlx.entity.UserDO;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

/**
 *  用户映射器
 * @author 熊新欣
 * @date 2020/8/4
 */
@Repository
public interface UserMapper {
    /**
     * 验证登录
     * @param user 登录对象
     * @return 用户对象
     */
    UserDO doLogin(UserDO user);

    /**
     * 注册
     * @param user 用户对象
     * @return 插入的数量
     */
    Integer insertUser(UserDO user);

    /**
     * 根据id获取用户对象
     * @param id 用户对象
     * @return 用户对象
     */
    UserDO getUserById(Integer id);

    /**
     * 更改头像
     * @param userDO 新头像地址
     */
    void updateUserIcon(UserDO userDO);

    /**
     * 更改背景图片
     * @param userDO 背景图片地址
     */
    void updateUserBackgroundPicture(UserDO userDO);

    /**
     * 验证手机或邮箱是否存在
     * @param userDO 手机号或邮箱
     * @return 用户
     */
    UserDO isValid(UserDO userDO);
}
