package hzvtc.zlx.dao;

import hzvtc.zlx.entity.FoodDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 求琰锋
 * @date 2020/8/24
 */
@Repository
public interface FoodMapper {
    /**
     * 插入美食
     * @return 插入的数量
     * @param food 美食
     */
    Integer insertFood(FoodDO food);

    /**
     * 获取文章列表
     * @return 文章列表
     */
    List<FoodDO> listFoods();

    /**
     * 根据id获取文章
     * @return 文章
     * @param id 文章id
     */
    FoodDO getFoodById(Integer id);
}
