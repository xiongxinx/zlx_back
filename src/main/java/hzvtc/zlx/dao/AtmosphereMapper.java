package hzvtc.zlx.dao;

import hzvtc.zlx.entity.AtmosphereDO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AtmosphereMapper {
    /**
     * 获取城市文化列表
     * @return 城市文化列表
     */
    List<AtmosphereDO> listAtmosphere();
}
