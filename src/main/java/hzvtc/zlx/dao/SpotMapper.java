package hzvtc.zlx.dao;

import hzvtc.zlx.entity.SpotDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/12
 */
@Repository
public interface SpotMapper {
    /**
     * 根据城市id获取景点列表
     * @param id 城市id
     * @return 景点列表
     */
    List<SpotDO> getSpotByCityId(Integer id);

    /**
     * 根据id获取景点
     * @param id 景点id
     * @return 景点实体类
     */
    SpotDO getSpotById(Integer id);
}
