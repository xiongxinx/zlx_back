package hzvtc.zlx.dao;

import hzvtc.zlx.entity.CollectionDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/7
 */
@Repository
public interface CollectionMapper {
    /**
     * 添加收藏
     * @param collectionDO 用户id 和 文章id
     */
    void insertCollection(CollectionDO collectionDO);
    /**
     * 根据用户id获取收藏列表
     * @param id 用户id
     * @return 收藏列表
     */
    List<CollectionDO> listCollectionByUserId(Integer id);

    /**
     * 根据文章id获取被文章收藏数量
     * @param id 文章id
     * @return 被收藏数量
     */
    Integer countCollectionByArticleId(Integer id);

    /**
     * 根据文章id 和 用户id 删除收藏
     * @param collectionDO 用户id 和 文章id
     */
    void deleteCollectionByArticleIdAndUserId(CollectionDO collectionDO);

    /**
     * 是否收藏
     * @param articleId 文章id
     * @param userId 用户id
     * @return 记录
     */
    CollectionDO saved(@Param("articleId") Integer articleId,@Param("userId") Integer userId);
}
