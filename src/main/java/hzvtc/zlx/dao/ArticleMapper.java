package hzvtc.zlx.dao;

import hzvtc.zlx.entity.ArticleDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Repository
public interface ArticleMapper {
    /**
     * 插入文章
     * @return 插入的数量
     * @param article 文章
     */
    Integer insertArticle(ArticleDO article);

    /**
     * 获取文章列表
     * @return 文章列表
     */
    List<ArticleDO> listArticle();

    /**
     * 根据id获取文章
     * @return 文章
     * @param id 文章id
     */
    ArticleDO getArticleById(Integer id);

    /**
     * likeNumber+1
     * @param id 文章id
     */
    void updateLikeNumberAdd(Integer id);

    /**
     *  likeNumber-1
     * @param id 文章id
     */
    void updateLikeNumberSub(Integer id);

    /**
     * 根据用户id获取文章列表
     * @param id 用户id
     * @return 文章列表
     */
    List<ArticleDO> listArticleByUserId(Integer id);
}
