package hzvtc.zlx.dao;

import hzvtc.zlx.entity.CityDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 熊新欣
 * @date 2020/8/5
 */
@Repository
public interface CityMapper {
    /**
     * 根据名字获取城市
     * @return 城市
     * @param name 城市名字
     */
    CityDO getCityByName(String name);

    /**
     * 获取城市列表
     * @return 城市列表
     */
    List<CityDO> listCity();
}
