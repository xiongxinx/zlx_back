package hzvtc.zlx;

import com.alibaba.fastjson.JSON;
import hzvtc.zlx.dto.AtmosphereDTO;
import hzvtc.zlx.entity.AtmosphereDO;
import hzvtc.zlx.service.AtmosphereService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.List;

@SpringBootTest
public class JsonTest {

    @Autowired
    public AtmosphereService atmosphereService;

    @Test
    public void Tests() {
        System.out.println("测试程序");
        try {
            String filePathh = "E:/upload/data2.json";//json文件地址
            InputStream inputStream = new FileInputStream(filePathh);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
                sb.append(line);
            }
            List<AtmosphereDO> poets = JSON.parseArray(sb.toString(), AtmosphereDO.class);
            for (AtmosphereDO poet : poets) {
                System.out.println(poet.getCityCultureName());
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void Test1s() {
        System.out.println("测试程序");
        List<AtmosphereDTO> atmosphereDOS = atmosphereService.list();
        for (AtmosphereDTO poet : atmosphereDOS) {
            System.out.println(poet);
        }
    }
}
